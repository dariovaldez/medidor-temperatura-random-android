package com.dartcode.temperaturadistribuidos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity {

    private static Socket socket;
    private static String ip = "192.168.1.136";
    private static int puerto = 5000;
    private static PrintWriter printWriter;

    Button btnContinuar, btnParar;
    TextView txtValorTemperatura;
    int valor;
    String temperatura;
    Timer timer;
    TimerTask tarea;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtValorTemperatura = findViewById(R.id.txtValorTemperatura);
        btnContinuar = findViewById(R.id.btnComenzar);
        btnParar = findViewById(R.id.btnParar);


        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnParar.setEnabled(true);
                btnContinuar.setEnabled(false);
                MedirTemperatura();
            }
        });

        btnParar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnContinuar.setEnabled(true);
                btnParar.setEnabled(false);
                PararTemperatura();
            }
        });


    }


    private void MedirTemperatura() {
        timer = new Timer();
        tarea = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Random aleatorio = new Random();
                        valor = aleatorio.nextInt(45);
                        temperatura = String.valueOf(valor);
                        txtValorTemperatura.setText(temperatura + " ºC");
                        EnviarServerSocket enviar = new EnviarServerSocket();
                        enviar.execute();
                    }
                });

            }
        };timer.scheduleAtFixedRate(tarea, 0, 2000);
    }

    private void PararTemperatura() {
        tarea.cancel();
    }

    private class EnviarServerSocket extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                socket = new Socket(ip,puerto);
                printWriter = new PrintWriter(socket.getOutputStream());
                printWriter.write(temperatura);
                printWriter.flush();
                printWriter.close();
                socket.close();
                Toast.makeText(getApplicationContext(), "Dato enviado al server", Toast.LENGTH_SHORT);
            }catch (Exception e){

                Log.e("socket", "No pudo enviarse");

            }

            return null;
        }
    }

}